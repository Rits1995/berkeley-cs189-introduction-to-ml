
# UC Berkeley Course CS189 - Introduction to Machine Learning (Spring 2019)


Lecture notes, syllabus, staff, schedule, projects and other materials can be viewd from the [course webpage](https://people.eecs.berkeley.edu/~jrs/189/).
 - Discussion: [Piazza](https://piazza.com/class/jpdq1epqrr3231)
 - Schedule: [Berkeley CS189](https://classes.berkeley.edu/content/2019-spring-compsci-189-001-lec-001)


## Prerequisites

If you want to brush up on prerequisite material, Stanford's machine learning class provides nice reviews of linear algebra and probability theory. You should take these prerequisites quite seriously: if you don't have them, I strongly recommend not taking CS 189.

 - Math 53 (or another vector calculus course),
 - Math 54, Math 110, or EE 16A+16B (or another linear algebra course)
 - CS 70, EECS 126, or Stat 134 (or another probability course)


## Topics

This class introduces algorithms for learning, which constitute an important part of artificial intelligence.
 - Classification: perceptrons, support vector machines (SVMs), Gaussian discriminant analysis (including linear discriminant analysis, LDA, and quadratic discriminant analysis, QDA), logistic regression, decision trees, neural networks, convolutional neural networks, boosting, nearest neighbor search;
 - Regression: least-squares linear regression, logistic regression, polynomial regression, ridge regression, Lasso;
 - Density estimation: maximum likelihood estimation (MLE);
 - Dimensionality reduction: principal components analysis (PCA), random projection, latent factor analysis; and
 - Clustering: k-means clustering, hierarchical clustering, spectral graph clustering.
